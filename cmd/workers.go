package cmd

import (
	"fmt"
	"log/slog"
	"os"
	"path"
	"path/filepath"

	"golang.org/x/sync/errgroup"
)

// https://artyom.dev/goroutine-control
// https://gist.github.com/pteich/c0bb58b0b7c8af7cc6a689dd0d3d26ef

const (
	noCacheDir = ""
)

func startBuilders(queue chan string, consumers int) (err error) {
	return startCachedBuilders(queue, consumers, noCacheDir)
}

func startCachedBuilders(queue chan string, consumers int, cacheDir string) (err error) {
	var eg errgroup.Group
	eg.SetLimit(consumers)

	for i := 0; i < consumers; i++ {
		eg.Go(func() error {
			for courseDir := range queue {
				c, err := NewCourseWithDir(courseDir)
				if err != nil {
					slog.Error(
						"failed to read course from options",
						slog.String("course", path.Base(courseDir)),
						slog.String("error", err.Error()),
					)
					continue
				}
				c.CacheDir = cacheDir
				err = c.BuildOutline()
				if err != nil {
					slog.Error(
						"failed to build course outline",
						slog.String("course", path.Base(courseDir)),
						slog.String("error", err.Error()),
					)
				}
			}
			return nil
		})
	}

	_ = eg.Wait()
	return nil
}

func readCourses(coursesDir string) (c chan string, err error) {
	c = make(chan string, 1)
	items, err := os.ReadDir(coursesDir)
	if err != nil {
		err = fmt.Errorf("failed to find files in courses dir %s: %w", coursesDir, err)
		return c, err
	}
	go func() {
		defer close(c)
		for _, item := range items {
			if item.IsDir() {
				c <- filepath.Join(coursesDir, item.Name())
			}
		}
	}()
	return c, nil
}
