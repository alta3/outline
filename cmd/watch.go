package cmd

import (
	"fmt"
	"log/slog"
	"os"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

const(
	cacheDir = "/tmp/outline_cache"

	// flag names
	intervalFlag    = "interval"
	defaultInterval = 30
)

func buildTrigger(done chan struct{}, interval int) {
	time.Sleep(time.Second * time.Duration(interval))
	done <- struct{}{}
}

// watchCmd represents the watch command
var watchCmd = &cobra.Command{
	Use:   "watch [courses directory]",
	Short: "Build all courses in a directory, at interval",
	Args:  cobra.ExactArgs(1),

	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		viper.SetEnvPrefix(outlineEnvPrefix)
		viper.AutomaticEnv()

		viperPersistentFlags(cmd)

		viper.SetDefault(intervalFlag, defaultInterval)
		viper.BindPFlag(intervalFlag, cmd.Flags().Lookup(intervalFlag))
	},

	RunE: func(cmd *cobra.Command, args []string) (err error) {

		err = os.MkdirAll(cacheDir, os.ModeDir)
		if err != nil {
			return fmt.Errorf("failed to create outline cache dir: %w", err)
		}
		defer os.RemoveAll(cacheDir)

		for {
			queue, err := readCourses(args[0])
			if err != nil {
				slog.Error("error reading courses",
					slog.String("courses", args[0]),
					slog.String("error", err.Error()),
				)
			}
			err = startCachedBuilders(queue, workers, cacheDir)
			if err != nil {
				slog.Error("error building outlines",
					slog.String("courses", args[0]),
					slog.String("error", err.Error()),
				)
			}

			wait := make(chan struct{})
			interval := viper.GetInt(intervalFlag)
			go buildTrigger(wait, interval)
			<-wait
		}
	},
}

func init() {
	watchCmd.Flags().IntP(
		intervalFlag, "n", 30,
		"seconds to wait between restart")
	rootCmd.AddCommand(watchCmd)
}
