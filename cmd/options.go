package cmd

import (
	"fmt"
	"net/url"
	"os"
	"path/filepath"

	"github.com/spf13/viper"
	"gitlab.com/alta3/outline/internal/course"
)

func errFlagNotSet(flag string) (err error) {
	return fmt.Errorf("required flag %q not set", flag)
}

// Reads all configuration for course/outline build
// No override courseDir, read from config flag/env only
func NewCourse() (c *course.Course, err error) {
	return NewCourseWithDir("")
}

// Reads all configuration for course/outline build
// courseDir parameter allows for build-all style building all 
// courses within a containing directory.
func NewCourseWithDir(courseDir string) (c *course.Course, err error) {
	c = &course.Course{}

	// Check for paramter input, if empty read from viper
	if courseDir == "" {
		courseDir = viper.GetString(courseFlag)
	}
	// If viper config is empy, error
	if courseDir == "" {
		return c, errFlagNotSet(courseFlag)
	}
	c.CourseDir = courseDir
	courseDirStat, err := os.Stat(c.CourseDir)
	if err != nil {
		return c, fmt.Errorf("course flag directory not found: %w", err)
	}
	if !courseDirStat.IsDir() {
		return c, fmt.Errorf("course flag directory is not a directory")
	}

	fragmentsDir := viper.GetString(fragmentsFlag)
	c.OutlineDir = filepath.Join(c.CourseDir, fragmentsDir)
	fragmentsDirStat, err := os.Stat(c.OutlineDir)
	if err != nil {
		return c, fmt.Errorf("outline fragment directory not found: %w", err)
	}
	if !fragmentsDirStat.IsDir() {
		return c, fmt.Errorf("outline fragment directory is not a directory")
	}

	c.OutputDir = viper.GetString(outputFlag)
	if c.OutputDir == "" {
		return c, errFlagNotSet(outputFlag)
	}
	outputDir, err := os.Stat(c.OutputDir)
	if err != nil {
		return c, fmt.Errorf("output flag directory not found: %w", err)
	}
	if !outputDir.IsDir() {
		return c, fmt.Errorf("output flag directory is not a directory")
	}

	// default shortcode derived from course directory path
	c.ShortCode = viper.GetString(shortcodeFlag)
	if c.ShortCode == "" {
		inDir := filepath.Base(c.CourseDir)
		if inDir == "." {
			return c, fmt.Errorf("bad shortcode %s", c.ShortCode)
		}
		c.ShortCode = inDir
	}

	staticURL := viper.GetString(staticFlag)

	c.CourseIcon, err = url.Parse(fmt.Sprintf("%s/%s/%s.png",
		staticURL, c.ShortCode, c.ShortCode))
	if err != nil {
		return c, fmt.Errorf("failed to parse course icon url: %w", err)
	}

	logoFile := viper.GetString(logoFlag)
	c.CompanyIcon, err = url.Parse(fmt.Sprintf("%s/%s",
		staticURL, logoFile))
	if err != nil {
		return c, fmt.Errorf("failed to parse logo url: %w", err)
	}

	return c, nil
}
