package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

const (
	// flag names
	coursesFlag = "courses"

	workers = 10
)

// buildAllCmd represents the buildAll command
var buildAllCmd = &cobra.Command{
	Use:   "build-all [courses directory]",
	Short: "Build all courses in a directory",
	Args:  cobra.ExactArgs(1),

	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		viper.SetEnvPrefix(outlineEnvPrefix)
		viper.AutomaticEnv()

		viperPersistentFlags(cmd)
	},

	RunE: func(cmd *cobra.Command, args []string) (err error) {
		queue, err := readCourses(args[0])
		if err != nil {
			return 
		}
		err = startBuilders(queue, workers)
		if err != nil {
			return 
		}
		return nil
	},
}


func init() {
	rootCmd.AddCommand(buildAllCmd)
}
