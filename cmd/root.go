package cmd

import (
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

const (
	// intentionally explain where default value comes from
	// if set to a specific default, the viper default will be ignored
	defaultFromViper = ""

	outlineEnvPrefix = "OUTLINE"

	// flag names
	staticFlag    = "static"
	logoFlag      = "logo"
	fragmentsFlag = "fragments"
	debugFlag     = "debug"

	// defaults
	defaultStatic       = "https://static.alta3.com/outlines"
	defaultCompanyLogo  = "alta3.png"
	defaultFragmentsDir = "00-outline"
)

var rootCmd = &cobra.Command{
	Use:   "outline",
	Short: "Build a pdf/docx from outline component md files",
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		viper.AutomaticEnv()
		viper.SetEnvPrefix(outlineEnvPrefix)
		viperPersistentFlags(cmd)
	},
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func viperPersistentFlags(cmd *cobra.Command) {
	viper.SetDefault(staticFlag, defaultStatic)
	viper.BindPFlag(staticFlag, cmd.PersistentFlags().Lookup(staticFlag))

	viper.SetDefault(logoFlag, defaultCompanyLogo)
	viper.BindPFlag(logoFlag, cmd.PersistentFlags().Lookup(logoFlag))

	viper.SetDefault(fragmentsFlag, defaultFragmentsDir)
	viper.BindPFlag(fragmentsFlag, cmd.PersistentFlags().Lookup(fragmentsFlag))

	viper.SetDefault(outputFlag, outputDefault)
	viper.BindPFlag(outputFlag, cmd.Flags().Lookup(outputFlag))
}

func init() {

	// Persistenet flags, available on all subcommands
	rootCmd.PersistentFlags().String(
		staticFlag, defaultFromViper,
		"Static URL for retrieving logo files")
	rootCmd.PersistentFlags().String(
		logoFlag, defaultFromViper,
		"Path to company logo icon in static path")
	rootCmd.PersistentFlags().String(
		fragmentsFlag, defaultFromViper,
		"Outline fragments directory (inside course dir)")
	rootCmd.PersistentFlags().StringP(
		outputFlag, "o", defaultFromViper,
		"Path to output directory for outline artifacts (pdf/docx)")
}
