package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

const (
	// flag names
	courseFlag    = "course"
	outputFlag    = "output"
	outputDefault = "."
	shortcodeFlag = "shortcode"
)

// buildCmd represents the "build" command
var buildCmd = &cobra.Command{

	Use:   "build",
	Short: "Build a pdf/docx from outline component md files",

	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		viper.SetEnvPrefix(outlineEnvPrefix)
		viper.AutomaticEnv()

		viperPersistentFlags(cmd)

		viper.BindPFlag(courseFlag, cmd.Flags().Lookup(courseFlag))
		viper.BindPFlag(shortcodeFlag, cmd.Flags().Lookup(shortcodeFlag))
	},

	RunE: func(cmd *cobra.Command, args []string) (err error) {
		c, err := NewCourse()
		if err != nil {
			return err 
		}
		return c.BuildOutline()
	},
}

func init() {

	buildCmd.Flags().StringP(
		courseFlag, "c", defaultFromViper,
		"Path to courseFlag directory for outline building")
	buildCmd.Flags().StringP(
		shortcodeFlag, "s", defaultFromViper,
		"courseFlag short code")

	rootCmd.AddCommand(buildCmd)
}
