FROM golang:1.21.4-bookworm as build-stage

WORKDIR /app
COPY . .
RUN go mod download
RUN go build -o /outline .

FROM dalibo/pandocker:latest-ubuntu-full

RUN apt-get -qq update \
	&& apt-get -qy install --no-install-recommends \
		fonts-symbola
COPY --from=build-stage /outline /outline
VOLUME /pandoc
WORKDIR /pandoc
ENTRYPOINT ["/outline"]
