package course

import (
	"net/url"
	"os"
	"time"
)

type CourseMetadata struct {
	CourseTitle string `yaml:"marketing_name"`
}

type Course struct {
	CourseTitle string
	ShortCode   string
	CourseIcon  *url.URL
	CompanyIcon *url.URL
	CourseDir   string
	OutlineDir  string
	OutputDir   string
	CacheDir    string
	Version     string
	Outline
}

type Outline struct {
	SUMMARY        *Fragment //  course dir / SUMMARY.md
	CourseMetadata *Fragment // outline dir / course.yml
	Specs          *Fragment // outline dir / specs.md
	Overview       *Fragment // outline dir / overview.md
	Audience       *Fragment // outline dir / audience.md
	Objectives     *Fragment // outline dir / objectives.md
	Prereq         *Fragment // outline dir / prereq.md
	NextCourses    *Fragment // outline dir / next-courses.md
	Cert           *Fragment // outline dir / cert.md
}

type HashDate struct {
	Hash string
	Date time.Time
}

type Fragment struct {
	os.FileInfo
	Path     string
	Header   string
	HashDate
}
