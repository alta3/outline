package course

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
)

const(
	pdfEngine = "xelatex"
	pdfFont =  "Symbola"
	docxFont = "Symbola"
	margin = "1in"
)

func (c *Course) PandocPDF(md string) (err error) {
	outFilename := fmt.Sprintf("%s.pdf", c.ShortCode)
	courseDir := filepath.Join(c.OutputDir, c.ShortCode)
	err = os.MkdirAll(courseDir, os.ModeDir)
	if err != nil {
		return err
	}
	outputPath := filepath.Join(courseDir, outFilename)
	pandocCmd := exec.Command("pandoc",
		fmt.Sprintf("--pdf-engine=%s", pdfEngine),
		fmt.Sprintf("--variable=mainfont:%s", pdfFont),
		fmt.Sprintf("--variable=geometry:margin=%s", margin),
		"--from=markdown",
		"--to=pdf",
		"--verbose",
		fmt.Sprintf("--output=%s", outputPath),
		md,
	)
	out, err := pandocCmd.CombinedOutput()
	if err != nil {
		// fmt.Println(pandocCmd.Args)
		errorFilename := fmt.Sprintf("%s.pdf.error.log", c.ShortCode)
		errorPath := filepath.Join(c.OutputDir, errorFilename)
		err := fmt.Errorf("pandoc exec failed - see %s for full log: %w", errorPath, err)
		_ = os.WriteFile(errorPath, out, 0666)
		return err
	}
	_ = os.Remove(md)
	return nil
}

func (c *Course) PandocDOCX(md string) (err error) {
	outFilename := fmt.Sprintf("%s.docx", c.ShortCode)
	courseDir := filepath.Join(c.OutputDir, c.ShortCode)
	err = os.MkdirAll(courseDir, os.ModeDir)
	if err != nil {
		return err
	}
	outputPath := filepath.Join(courseDir, outFilename)
	pandocCmd := exec.Command("pandoc",
		fmt.Sprintf("--variable=mainfont:%s", docxFont),
		fmt.Sprintf("--variable=geometry:margin=%s", margin),
		"--from=markdown",
		"--to=docx",
		"--verbose",
		fmt.Sprintf("--output=%s", outputPath),
		md,
	)
	out, err := pandocCmd.CombinedOutput()
	if err != nil {
		// fmt.Println(pandocCmd.Args)
		errorFilename := fmt.Sprintf("%s.docx.error.log", c.ShortCode)
		errorPath := filepath.Join(c.OutputDir, errorFilename)
		err := fmt.Errorf("pandoc exec failed - see %s for full log: %w", errorPath, err)
		_ = os.WriteFile(errorPath, out, 0666)
		return err
	}
	_ = os.Remove(md)
	return nil
}
