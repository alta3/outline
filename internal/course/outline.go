package course

import (
	"bytes"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"time"

	"gitlab.com/alta3/outline/internal/git"
	"gopkg.in/yaml.v3"
)

const (
	overview = "Course Overview"
)

func getFragment(dir, filename, header string) (f *Fragment, err error) {
	f = &Fragment{}
	f.Path = filepath.Join(dir, filename)
	stat, err := os.Stat(f.Path)
	if err != nil {
		return nil, nil
	}
	if stat.IsDir() {
		return nil, nil
	}
	f.FileInfo = stat
	f.Header = header
	f.HashDate.Hash, f.HashDate.Date, err = git.FileLastChange(f.Path)
	if err != nil {
		return nil, err
	}
	return f, nil
}

// _Maybe_ move fragmentTitles into a map[string]string
// set default values and read from env with default
func (c *Course) CollectOutline() (err error) {
	c.Outline = Outline{}
	var latest HashDate = HashDate{Date: time.Time{}}

	c.SUMMARY, err = getFragment(c.CourseDir, "SUMMARY.md", "Outline")
	if err != nil {
		return err
	}
	if c.SUMMARY != nil && c.SUMMARY.Date.After(latest.Date) {
		latest = c.SUMMARY.HashDate
	}

	c.Specs, err = getFragment(c.OutlineDir, "specs.md", "")
	if err != nil {
		return err
	}
	if c.Specs != nil && c.Specs.Date.After(latest.Date) {
		latest = c.Specs.HashDate
	}

	c.Overview, err = getFragment(c.OutlineDir, "overview.md", "Course Overview")
	if err != nil {
		return err
	}
	if c.Overview != nil && c.Overview.Date.After(latest.Date) {
		latest = c.Overview.HashDate
	}

	c.Audience, err = getFragment(c.OutlineDir, "audience.md", "Audience")
	if err != nil {
		return err
	}
	if c.Audience != nil && c.Audience.Date.After(latest.Date) {
		latest = c.Audience.HashDate
	}

	c.Objectives, err = getFragment(c.OutlineDir, "objectives.md", "Objectives")
	if err != nil {
		return err
	}
	if c.Objectives != nil && c.Objectives.Date.After(latest.Date) {
		latest = c.Objectives.HashDate
	}

	c.Prereq, err = getFragment(c.OutlineDir, "prereq.md", "Optional Prerequisites")
	if err != nil {
		return err
	}
	if c.Prereq != nil && c.Prereq.Date.After(latest.Date) {
		latest = c.Prereq.HashDate
	}

	c.NextCourses, err = getFragment(c.OutlineDir, "next-courses.md", "Next Courses")
	if err != nil {
		return err
	}
	if c.NextCourses != nil && c.NextCourses.Date.After(latest.Date) {
		latest = c.NextCourses.HashDate
	}

	c.Cert, err = getFragment(c.OutlineDir, "cert.md", "Certification")
	if err != nil {
		return err
	}
	if c.Cert != nil && c.Cert.Date.After(latest.Date) {
		latest = c.Cert.HashDate
	}

	c.Version = fmt.Sprintf("%s %s", latest.Hash, latest.Date.Format("2006-01-02"))
	return nil
}

func (c *Course) GetCourseTitle() string {
	courseYAML := filepath.Join(c.CourseDir, "course.yml")
	data, err := os.ReadFile(courseYAML)
	if err != nil {
		return ""
	}
	var course CourseMetadata
	err = yaml.Unmarshal(data, &course)
	if err != nil {
		return ""
	}
	return course.CourseTitle
}

func (c *Course) BuildOutline() (err error) {
	err = c.CollectOutline()
	if err != nil {
		return err
	}
	c.CourseTitle = c.GetCourseTitle()

	if !remoteFileExits(*c.CourseIcon) {
		return fmt.Errorf("course icon not found at %s", c.CourseIcon)
	}
	if !remoteFileExits(*c.CompanyIcon) {
		return fmt.Errorf("company icon not found at %s", c.CompanyIcon)
	}

	mdPDF, err := c.GenerateSourcePDF()
	if err != nil {
		return err
	}

	mdDOCX, err := c.GenerateSourceDOCX()
	if err != nil {
		return err
	}

	var build bool = true
	if c.CacheDir != "" {
		build = false
		cachedName := fmt.Sprintf("%s_pdf.md", c.ShortCode)
		cachedPath := filepath.Join(c.CacheDir, cachedName)
		changed, _ := fileChanged(mdPDF, cachedPath)
		if changed {
			err := fileCache(mdPDF, cachedPath)
			if err != nil {
				return fmt.Errorf("failed to write cached md: %w", err)
			}
			build = true
		} 
	}
	if build {
		err = c.PandocPDF(mdPDF)
		if err != nil {
			return err
		}
		err = c.PandocDOCX(mdDOCX)
		if err != nil {
			return err
		}
	}
	return nil
}

// Could be optimized, but brute force is functional
func fileChanged(f1, f2 string) (changed bool, err error) {
	f1b, err := os.ReadFile(f1)
	if err != nil {
		return true, err
	}
	f2b, err := os.ReadFile(f2)
	if err != nil {
		return true, err
	}
	return !bytes.Equal(f1b, f2b), nil
}

func fileCache(f1, f2 string) (err error) {
	b, err := os.ReadFile(f1)
	if err != nil {
		return err
	}
	err = os.WriteFile(f2, b, 0666)
	if err != nil {
		return err
	}
	return nil
}

func remoteFileExits(fileURL url.URL) bool {
	timeout := time.Duration(1 * time.Second)
	client := http.Client{
		Timeout: timeout,
	}
	resp, err := client.Get(fileURL.String())
	if err != nil {
		return false
	}
	if resp.StatusCode != http.StatusOK {
		return false
	}
	return true
}
