package course

import (
	"bytes"
	"fmt"
	"html/template"
	"os"
	"regexp"
)

const(
	tmpDir = "/tmp"
)

// TODO: Move to files
var (
	outlineTemplate string = `{{ template "header" . }}
# {{ .CourseTitle }}
{{ content .Specs }}
{{ content .Overview }}
{{ content .Audience }}
{{ content .Objectives }}
{{ marketing .SUMMARY }}
{{ content .Prereq }}
{{ content .NextCourses }}
{{ content .Cert }}
{{ template "footer" . }}`

	pdfTemplate string = `{{ define "header" }}
![]({{ .CourseIcon }}){height=75px}\ \hfill ![]({{ .CompanyIcon }}){height=75px}
{{ end }}
{{ define "footer" }}
\vspace{\fill}
\begin{center}{{ .Version }}\end{center}
{{ end }}`

	docxTemplate string = `{{ define "header" }}
![]({{ .CourseIcon }}){height=75px}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![]({{ .CompanyIcon }}){height=75px}
{{ end }}
{{ define "footer" }}
\n\n{{ .Version }}
{{ end }}`
)

func Content(f *Fragment) template.HTML {
	if f == nil {
		return ""
	}
	b, err := os.ReadFile(f.Path)
	if err != nil {
		return ""
	}
	var h []byte
	if f.Header != "" {
		h = []byte("## " + f.Header + "\n")
	}
	h = append(h, b...)
	return template.HTML(h[:])
}

func extractMarketingContent(data []byte) []byte {
	startMarker := []byte("<!--- MARKETING START --->")
	endMarker := []byte("<!--- MARKETING END --->")

	startIndex := bytes.Index(data, startMarker)
	if startIndex == -1 {
		startIndex = 0
	}

	// adjust the start index to begin after the start marker
	startIndex += len(startMarker)

	endIndex := bytes.Index(data, endMarker)
	if endIndex == -1 {
		endIndex = len(data)
	}

	// extract the content between the markers
	return data[startIndex:endIndex]
}

func MarketingContent(f *Fragment) template.HTML {
	if f == nil {
		return ""
	}
	b, err := os.ReadFile(f.Path)
	if err != nil {
		return ""
	}
	var h []byte
	if f.Header != "" {
		h = []byte("## " + f.Header + "\n")
	}
	linksReg := regexp.MustCompile(`\[(.*?)\][\[\(].*?[\]\)]`)
	noLinks := linksReg.ReplaceAllString(string(b), "$1")

	headerReg := regexp.MustCompile(`(?m)^\#{1,6}\s*([^#]+)\s*(\#{1,6})?$`)
	headersFixed := headerReg.ReplaceAllString(noLinks, "#### $1")

	laptop := `- \x{1F4BB}` //💻
	speech := `- \x{1F4AC}` //💬
	trophy := `- \x{1F3C6}` //🏆
	labsReg := regexp.MustCompile(laptop)
	emojiExplained1 := labsReg.ReplaceAllString(headersFixed, "- 💻 Lab:")
	lectureReg := regexp.MustCompile(speech)
	emojiExplained2 := lectureReg.ReplaceAllString(emojiExplained1, "- 💬 Lecture:")
	challengeReg := regexp.MustCompile(trophy)
	emojiExplained3 := challengeReg.ReplaceAllString(emojiExplained2, "- 🏆 Challenge:")

	marketingContent := extractMarketingContent([]byte(emojiExplained3))

	h = append(h, []byte(marketingContent)...)
	return template.HTML(h[:])
}

func (c *Course) GenerateSourcePDF() (path string, err error) {
	funcs := template.FuncMap{
		"content":   Content,
		"marketing": MarketingContent,
	}
	tmpl, err := template.New("outline").Funcs(funcs).Parse(outlineTemplate)
	if err != nil {
		return path, fmt.Errorf("failed to parse outline template: %w", err)
	}
	pdfTmpl, err := template.Must(tmpl.Clone()).Parse(pdfTemplate)
	if err != nil {
		return path, fmt.Errorf("failed to parse pdf outline template: %w", err)
	}
	md, err := os.CreateTemp(tmpDir, "*_pdf.md")
	if err != nil {
		return path, fmt.Errorf("failed to create pdf outline template: %w", err)
	}
	err = pdfTmpl.Execute(md, c)
	if err != nil {
		return path, fmt.Errorf("failed to execute pdf outline template: %w", err)
	}
	return md.Name(), nil
}

func (c *Course) GenerateSourceDOCX() (path string, err error) {
	funcs := template.FuncMap{
		"content":   Content,
		"marketing": MarketingContent,
	}
	tmpl, err := template.New("outline").Funcs(funcs).Parse(outlineTemplate)
	if err != nil {
		return path, fmt.Errorf("failed to parse outline template: %w", err)
	}
	docxTmpl, err := template.Must(tmpl.Clone()).Parse(docxTemplate)
	if err != nil {
		return path, fmt.Errorf("failed to parse docx outline template: %w", err)
	}
	md, err := os.CreateTemp(tmpDir, "*_docx.md")
	if err != nil {
		return path, fmt.Errorf("failed to create docx outline template: %w", err)
	}
	err = docxTmpl.Execute(md, c)
	if err != nil {
		return path, fmt.Errorf("failed to execute docx outline template: %w", err)
	}
	return md.Name(), nil
}
