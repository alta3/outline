package git

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"

	"github.com/go-git/go-git/v5"
)

var gitRepos map[string]bool

func init() {
	gitRepos = make(map[string]bool)
}

func FindGit(path string) (dir, filePath string, err error) {
	// list of each directory up to path
	pathList := append([]string{string(os.PathSeparator)},
		strings.Split(path, string(os.PathSeparator))...)

	// search up the path
	for i := len(pathList); i > 0; i-- {

		dir := filepath.Join(pathList[0:i]...)
		stat, err := os.Stat(dir)
		if err != nil {
			continue
		}
		if !stat.IsDir() {
			continue
		}

		git := filepath.Join(dir, ".git")
		stat, err = os.Stat(git)
		if err != nil {
			continue
		}
		if !stat.IsDir() {
			continue
		}

		filePath = filepath.Join(pathList[i:]...)

		return dir, filePath, nil
	}
	err = fmt.Errorf("failed to find git directory in path %s: %w", path, err)
	return dir, filePath, err
}

func execGitRepo(path string) (err error) {
	if _, ok := gitRepos[path]; !ok {
		addSafe := exec.Command(
			"git", "config", "--global",
			"--add", "safe.directory",
			path)

		_, err := addSafe.CombinedOutput()
		if err != nil {
			return err
		}
		gitRepos[path] = true
	}
	return nil
}

// TODO exec 255 error
func execLastChange(path string) (hash string, date time.Time, err error) {
	repo, filePath, err := FindGit(path)
	if err != nil {
		return hash, date, err
	}
	if err := execGitRepo(repo); err != nil {
		return hash, date, err
	}
	logSingle := exec.Command(
		"git", "-C", repo, "log",
		"--max-count=1",
		"--pretty=format:%h %as",
		"--",
		filePath)
	out, err := logSingle.CombinedOutput()
	if err != nil {
		return hash, date, err
	}
	s := strings.Split(string(out), " ")
	if len(s) != 2 {
		return hash, date, fmt.Errorf("bad git output %s", string(out))
	}
	hash = s[0]
	date, err = time.Parse("2006-01-02", s[1])
	if err != nil {
		return hash, date, err
	}
	return hash, date, nil
}

// SLOW - not using - see issues
// https://github.com/go-git/go-git/issues/137
// https://github.com/go-git/go-git/issues/811
func gogitLastChange(path string) (hash string, date time.Time, err error) {
	dir, filePath, err := FindGit(path)
	if err != nil {
		return hash, date, err
	}
	r, err := git.PlainOpen(dir)
	if err != nil {
		return hash, date, err
	}
	ref, err := r.Head()
	if err != nil {
		return hash, date, err
	}
	fragmentLog := git.LogOptions{
		From:     ref.Hash(),
		FileName: &filePath,
		Order:    git.LogOrderDFS,
	}
	cIter, err := r.Log(&fragmentLog)
	if err != nil {
		return hash, date, err
	}
	// first =  latest
	commit, err := cIter.Next()
	if err != nil {
		return hash, date, err
	}
	hash = commit.Hash.String()[0:8]
	date = commit.Author.When
	return hash, date, err
}

func FileLastChange(path string) (hash string, date time.Time, err error) {
	return execLastChange(path)
}
