#!/bin/bash

# This script should be run from the /outline directory, using command `./scripts/push-outlines.sh`

# Loop through directories
for dir in outlines/*/; do
    # Get the last part of the directory name (course name) without the trailing '/'
    course=$(basename $dir)
    
    # Check if the local directory exists
    if [ -d "$dir" ]; then
        # Copy files to the remote server using SCP
        #BE SURE TO CHANGE .bravo to .alpha WHEN THE CLOUD CHANGES
        scp "$dir/$course.docx" "$dir/$course.pdf" enchilada.bravo:/opt/enchilada/run/static/outlines/$course/

        # Check if the SCP command was successful
        if [ $? -eq 0 ]; then
            echo "Updated $course with docx and pdf files"
        else
            echo "Error updating $course. SCP command failed."
        fi
    else
        echo "Local directory $dir does not exist."
    fi
done
