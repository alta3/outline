# `outline`

#### TODO: 

- [ ] move footer into actual footer docx
- [ ] viper/cobra for # of workers
- [ ] viper/cobra for liveness toggle (all or only live courses):
      --all (make all, default)
      --live (only course_state: live)

### Project Layout

```
cmd/
├── root.go     // cli `outline`           - only shows help
├── build.go    // cli `outline build`     - builds one outline
├── buildAll.go // cli `outline build-all` - builds all outlines within course dir
├── watch.go    // cli `outline watch`     - builds build-all continiuous
├── options.go  // helper functions for parsing viper/cobra env/flags
└── workers.go  // helper functions for running each outline build in goroutines

internal/
├── course // library for course structs / outline build operations
└── git    // library for git operations
```

## Before Building

Ensure local version of Go installed is at least 1.21.2

Install Symbola font

`sudo apt-get install fonts-symbola`

Install Pandoc

`sudo apt-get install pandoc`

Install Xetex

`sudo apt-get install texlive-xetex`

You need permission on every folder within outlines/

`chmod -R u+rwx ~/git/outline/outlines/`

Download and install golang
`sudo apt install golang`

## Build - binary

```bash
go mod download
go build .
./outline --help
```

### Run - binary

#### Build one course 

Note: Errors of exit status 255 occur on a number of courses, this does not seem to effect pdf/docx generation though.

```bash
./outline build --course [FULL-PATH]/labs/courses/kubernetes/ --output outlines/
```

#### Build all courses

```bash
./outline build-all [FULL-PATH]/labs/courses/ --output outlines/
```

#### Build all courses (watch)

```bash
./outline watch [FULL-PATH]/labs/courses/ --output outlines/
```

## Build - container

```bash
sudo docker build . -t outline:{{TAG}}
```

### Run - container

*Run from ~/git where your structure looks like:*

```
.
├── labs
├── outline
└── outlines
```

#### Build one course 

```bash
sudo docker run --rm -it \
  --volume "${PWD}/labs:/labs" \
  --volume "${PWD}/outlines:/outlines" \
  outline build --course /labs/courses/kubernetes/ --output /outlines/ 
```

#### Build all courses

```bash
sudo docker run --rm -it \
  --volume "${PWD}/labs:/labs" \
  --volume "${PWD}/outlines:/outlines" \
  outline build-all /labs/courses/ --output /outlines/
```

#### Build all courses (watch)

```bash
sudo docker run --rm -it \
  --volume "${PWD}/labs:/labs" \
  --volume "${PWD}/outlines:/outlines" \
  outline watch /labs/courses/ --output /outlines/
```

#### Push new outlines up to static

> **MAKE SURE YOU UPDATE THE SCRIPT TO POINT TO THE RIGHT CLOUD**

Then, from your machine in ~/git/outline run:

`~/git/outline$` `./scripts/push-outlines.sh`

#### Troubleshooting

Run container in foreground with bash entrypoint

```bash
sudo docker run --rm -it \
  --entrypoint /bin/bash \
  --volume "${PWD}/labs:/labs" \
  --volume "${PWD}/outlines:/outlines" \
  outline
```
