# How to install specific version of GO on Linux mint 21.2 (Victoria)

1. update and upgrade

    `sudo apt update`

    `sudo apt upgrade -y`

2. Because this ships directly with Mint, you will currently 2/7/2024 be at 1.18 with apt. That needs removed. Just remove all references of golang within your installed packages.

    `sudo apt remove '^golang-*'`

3. download the go version you want.

    `cd`

    `curl -OL https://go.dev/dl/go1.22.0.linux-amd64.tar.gz`

4. remove all references to other versions, then untar the the version you downloaded.

    `rm -rf /usr/local/go && tar -C /usr/local -xzf go1.22.0.linux-amd64.tar.gz`

5. Add the path to your profile.

    `echo 'export PATH=$PATH:/usr/local/go/bin' >> ~/.profile`

6. Source your profile.

    `source ~/.profile`

7. check your go version

    `go version`
